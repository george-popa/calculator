all: compile run
compile:
	gcc -Wall -o calculator calculator.c add.c substract.c
run:
	./calculator
clean:
	rm -f calculator 
