// Calculator.c
#include <stdio.h>
#include "add.h"
#include "substract.h"

int main() {
  
  int a = 10;
  int b = 2;

  printf("a = %d\n", a);
  printf("b = %d\n", b);

  int c;
  c = add(a, b);

  int d;
  d = substract(a, b);

  printf("a + b = %d\n", c);
  printf("a - b = %d\n", d);

  return 0;
}

